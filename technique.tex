\section{Our Transformation Techniques}

This chapter discusses the specifics of the optimizations we developed for this research, and how we arrived at them.

\subsection{Basis for Transformations}

We began by reasoning about common problems faced by symbolic execution and the structures that create these issues. We examined traditional symbolic execution, which uses time-limited depth-first search since complete symbolic execution is infeasible for programs of any significant size or complexity. Within this context, the goal is to maximize the amount of the program executed within the time limit. 

Firstly, we considered the behavior of branching control flow paths. Depth-first search favors longer paths over shorter ones, regardless of their content or the number of sub-branches therein. Deep paths with many branches therefore take up more time during symbolic execution. This is not necessarily best for overall code coverage, as more lines of code may be contained in shorter paths. 

Loops also cause significant trouble for symbolic execution~\cite{Godefroid:2011:APL:2001420.2001424},
%[TODO: cite others who tried to deal with loops], 
as symbolic depth-first search will execute the code within a loop as many times as is possible before examining any other part of the program. This can be a waste of time in terms of coverage, as no new statements are covered during that time if the loop iteration is not a factor in the control flow of the code contained therein.


%TODO: Discuss:
%Bounding loops (need a map from basic block to number of times visited... but just inserting a return block doesn't help. Unroll loops first? Something to do with this Loop Pass?)
%Causing sub-trees to immediately return. Maybe loop-heavy ones? Yeah, that seems sensible.
%Removing function calls
%For the case of if-repok-then-function-call (preconditions), do we cause the positive or negative branch to immediately return? Positive doesn't make the function call at all, negative, doesn't worry about what happens if !repok. 

%TODO: 
%Write LL repOk in C. Did that, it was awful. 
%Write manual examples (double loop, double conditional, which are working well)
%Difference from depth-limited execution: MUST get to a certain depth in the first loop AND the second, whereas slicing allows more executions to be dedicated to the first loop.
%run klee to completion on double_loop? Done. Took 10 hours.

\subsection{Transformation Design and Implementation: The Slicer}
\label{sec:the-slicer}

Based off of these observations, we created several new program transformations, which are applied at compile time. Each has the general goal of syntactically reducing, which we term as \emph{slicing}\footnote{Our use of the term slicing is different from the common use of this term in the context of \emph{program slicing} where a slice is created with respect to a given set of variables~\cite{SlicingSurvey}.}, a program to remove parts that have a low value proposition for symbolic execution. 
We introduce 5 slicing "modes" where each "mode" of our slicer is applied to the program under test on a function-by-function basis. 

Most modes first explore a function's control-flow graph to find its longest acyclic source-sink path, and use the length of this \emph{key path} as a guide while slicing the rest of the function. Identifying the key path can be a time-intensive process. It is a specific case of the \emph{longest path problem,} which is an NP-Hard problem.
It is at least fixed-parameter tractable in the length of the longest path $d$, meaning it can be solved in time $O(d!2^dn)$, where $n$ is the number of nodes in the graph~\cite{BODLAENDER19931}.

In our specific case, we are given a single source node from which to find paths in a directed graph, which significantly decreases the complexity. Our algorithms use depth-first search that breaks on cycles and are thus worst-case linear in the number of edges in the graph even without a priori knowledge of the length of the key path. However, as functions can have hundreds of edges, the time requirements can still increase beyond the point of feasibility. As such, we included the option to limit the amount of time the optimizer spends doing so in each function in the program under test. At the end of the specified time, the optimizer slices the function with regard to the longest path found in that time. Figures~\ref{fig:tech1},~\ref{fig:tech2},~\ref{fig:tech3},~\ref{fig:tech4}, and~\ref{fig:tech5} describe the five modes using pseudo-code.

Mode 1: The first of these transformations finds all acyclic source-sink paths in the function and slices each of them to half of their length. When path lengths are uneven (i.e. a path of length $X$ where $X$ mod 2 = 1) the optimizer leaves behind a path of length $ceiling(X/2)$. The same holds true throughout the slicer with regard to uneven path lengths. Due to state space explosion, the number of such paths can grow exponentially with the size of the program. Thus, this mode does not scale to real-world programs.

\begin{figure}[t]
\center
\begin{tabular}[c]{cc}
\begin{minipage}{\textwidth}
\center
\begin{lstlisting}[language=C]
identify all acyclic source-sink paths {
 recursively traverse graph using DFS
 upon finding a cycle or sink {
  if sink: store path, return
  if cycle: return
 }
 for each identified path {
   slice path in half {
    x = length of path/2
    l = length of path
    remove last l-x nodes from path
    add return block to end of path
 }
}
\end{lstlisting}\end{minipage}
\end{tabular}
\Caption{\label{fig:tech1} Pseudocode for Mode 1.}
\end{figure}


Mode 2: The second mode identifies the longest acyclic source-sink path and slices that path to half of its length. It then repeats this process until there is no acyclic source-sink path with length greater than half of the key path's original length or until time runs out. If the key path cannot be definitively identified in the allotted time, this mode is identical to the third. However, as discussed in Section~\ref{section:results}, this mode can produce superior results even with an identical time budget.

\begin{figure}[t]
\center
\begin{tabular}[c]{cc}
\begin{minipage}{\textwidth}
\center
\begin{lstlisting}
identify key path {
 while(time has not expired) {
  recursively traverse graph using DFS
  upon finding a cycle or sink {
   if sink: 
    if current path longer current longest path: 
     overwrite current longest path with 
      current path, return
    else return
   if cycle: return
  }
 }
}
l = length of key path
x = length of key path/2
slice key path {
    remove last l-x nodes from key path
    add return block to end of key path
}
while(time has not expired) {
    identify key path
    if key path length <= x: break
    slice key path
}
\end{lstlisting}\end{minipage}
\end{tabular}
\Caption{\label{fig:tech2} Pseudocode for Mode 2.}
\end{figure}


Mode 3: The third transformation is a reduced version of the second. It similarly identifies the key path and slices it in half, but stops there. This is a minor optimization, but it nonetheless provides a slight advantage for symbolic execution and fares well in some cases.

\begin{figure}[t]
\center
\begin{tabular}[c]{cc}
\begin{minipage}{\textwidth}
\center
\begin{lstlisting}
identify key path
slice key path
\end{lstlisting}\end{minipage}
\end{tabular}
\Caption{\label{fig:tech3} Pseudocode for Mode 3.}
\end{figure}


Mode 4: The fourth optimization works similarly to the second, but instead of slicing only acyclic source-sink paths, it blindly slices all paths to half of the key path's length. In essence, this mode ensures that there are no paths of length greater than $ceiling(X/2),$ where $X$ is the original length of the key path. This can be categorized as a more aggressive variant of traditional depth-limited symbolic execution. It removes nodes from the control flow graph entirely if there exists \emph{any} path that could reach them in $ceiling(X/2)$ or more traversals, meaning that nodes that could be reached in depth-limited execution through shorter paths can be removed from consideration in programs sliced using this mode.

\begin{figure}[t]
\center
\begin{tabular}[c]{cc}
\begin{minipage}{\textwidth}
\center
\begin{lstlisting}
identify key path
x = length of key path/2
blind slice {
 recursively traverse graph using DFS
 after making x traversals: direct all outgoing
  edges of current node to return blocks, return
}
\end{lstlisting}\end{minipage}
\end{tabular}
\Caption{\label{fig:tech4} Pseudocode for Mode 4.}
\end{figure}


Mode 5: The final transformation causes all loops to return after a single iteration. This massively decreases the number of paths through the graph and helps ensure that time is not wasted repeatedly executing the same section of code.

\begin{figure}[t]
\center
\begin{tabular}[c]{cc}
\begin{minipage}{\textwidth}
\center
\begin{lstlisting}
loop slice {
 while(time has not expired) {
  recursively traverse graph using DFS
  upon finding a cycle: direct cyclic outgoing
   edge of current node to return block, return
 }
}
\end{lstlisting}\end{minipage}
\end{tabular}
\Caption{\label{fig:tech5} Pseudocode for Mode 5.}
\end{figure}

The slicer also has a "Mode 0" which does not apply any transformations. This is a bookkeeping marker to identify the original program while keeping a consistent numbering scheme.