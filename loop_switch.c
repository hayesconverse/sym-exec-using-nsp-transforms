int main(int argc, char *argv[]) {
 int a = atoi(argv[1]);
 int x = 0, y = 0, z = 0;
 if (a < 0){
  return -1;
 }
 while(a < 12){
  if(a >= 0 && a < 4){
   x++;
   a++;
  }else if(a >=4 && a < 8){
   y++;
   a++;
  }else if(a >= 8 && a < 12){
   z++;
   a++;
  }
 }
 return z;
}