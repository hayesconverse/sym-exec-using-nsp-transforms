\section{Analysis}

\subsection{Overall Performance}

Across the 160 experimental trials in the non-cached version of the experiment, the optimized programs produced equivalent or superior results in 96 cases. Across the 160 trials in the cached version, the optimized programs did even better, producing equal- or higher-quality test suites in 103 cases. Table~\ref{table:progsimproved} shows the breakdown of the number of programs improved by each optimization. The combination of these cases provided improved coverage on 36 of the 40 tested Coreutils. In all but 8 trials where coverage was improved in each version of the experiment, the total time taken by the optimizer and KLEE to produce a test suite for a program was less than or equal to the time used for standard symbolic execution of the original program.

\begin{table*}
\centering
\begin{tabular}{||c|c|c||}
    \hline
    Slicer Mode & Executions Improved (Cache Disabled) & Executions Improved (Cache Enabled) \\
    \hline\hline
     Overall & 96 & 103\\
     \hline
     2 & 22 & 24 \\
     \hline
     3 & 18 & 21 \\
     \hline
     4 & 23 & 25 \\
     \hline
     5 & 33 & 33 \\
     \hline
\end{tabular}
\Caption{Number of Coreutils improved by each slicer mode}
\label{table:progsimproved}
\end{table*}

\subsection{Caching Disabled}

Table~\ref{table:avgcovgchanges} shows the average coverage as compared to unsliced programs for each mode overall, when coverage was improved, and when it was worse. Slicer mode 5, which causes loops to terminate after one execution, was by far the most effective of our optimizations, with many more improved cases and the highest average increase over the unaltered programs. Mode 3, which only slices a single path, was the least effective. This is understandable, as it is only a minor optimization, but it still outperformed the others in two trials, suggesting that there are cases where fewer alterations achieve superior results.

When coverage was worse, the amount by which it suffered varied significantly, from a minimum of 23.47\% of the unsliced program's test suite's coverage to a maximum of 97.7\%. On average, optimized programs that performed poorly had 73.12\% of their progenitor's coverage. 

\begin{table*}
\centering
\begin{tabular}{||c|c|c|c||}
    \hline
    Slicer Mode & Avg Coverage Change & Avg Coverage Gain Case & Avg Coverage Loss Case\\
    \hline\hline
     2 & 109.56 & 130.49 & 74.66 \\
     \hline
     3 & 95.59 & 112.53 & 72.82 \\
     \hline
     4 & 110.04 & 125.75 & 74.98 \\
     \hline
     5 & 123.08 & 134.96 & 67.11 \\
     \hline
\end{tabular}
\Caption{Average changes in coverage by slicer mode for Coreutils, cache disabled.}
\label{table:avgcovgchanges}
\end{table*}

The difference in the number of queries used by the sliced and unsliced versions of the program has a clear relationship to the change in coverage. Table~\ref{table:avgquerychanges} shows a comparison between the number of queries made during the execution of each sliced program to the number made executing the unsliced programs by mode. To reduce the impact of outliers, the single greatest and least values for each mode were removed from this calculation. A pattern is apparent in modes 2 and 4: both made more queries than the original program in successful trials and fewer in unsuccessful ones. Meanwhile, mode 3 consistently made fewer queries than either of its cousins.
Mode 5 breaks from this pattern: it made far fewer queries across all trials, issuing about half as many during successful trials and fewer during unsuccessful ones. 
%This, combined with mode 5's higher performance in terms of coverage, suggest that mode 5 is more effective at reducing the size of a program so as to better enable symbolic execution. This makes sense given that cyclic paths are often a program's longest and tend to be highly redundant. [TODO: Move this explanation to Technique section?] 

The optimizations all make fewer queries in unsuccessful trials on average; 51 of the 64 cases that produced inferior test suites used fewer queries than their unsliced originators (this ratio is consistent across all slicer modes within a 1-case tolerance).
%This is a natural weakness of the non-semantics-preserving nature of the transformations: if significant, complex portions of the program are removed and the remaining program is relatively small, even complete symbolic execution of the remainder is unlikely to cover the removed sections. Symbolic execution of the remainder in these cases takes far fewer queries. 

\begin{table*}
\centering
\begin{tabular}{||c|c|c|c||}
    \hline
    Slicer Mode & Avg Query Change & Avg Query Change (Coverage Gain) & Avg Query Change (Coverage Loss)\\
    \hline\hline
     Overall & 83.29 & 91.11 & 68.43\\
     \hline
     2 & 92.57 & 106.09 & 73.97\\
     \hline
     3 & 82.86 & 92.50 & 68.89\\
     \hline
     4 & 100.99 & 117.83 & 71.68\\
     \hline
     5 & 57.19 & 59.84 & 43.04\\
     \hline
\end{tabular}
\Caption{Average changes in queries by slicer mode for Coreutils, cache disabled.}
\label{table:avgquerychanges}
\end{table*}

%In the trials where coverage was not improved (52 of them), 26 times we had lower execution time in non-cached, and 26 times we had lower execution time in cached. Huh. 13 times we had more queries without caching. 19 times we had more queries without caching.

\subsection{Caching Enabled}

As referenced in Section~\ref{section:coreutilresults}, changes in line coverage were not large or consistent, but they were significant enough to increase the number of trials in which the optimized programs outperformed the non-optimized ones. In three cases, programs whose coverage was improved by optimization in the non-cached version did not see improvement in the cached version. In all of these cases, the original program had a larger increase in coverage than the optimized version with the addition of caching. Broadly speaking, caching made the optimizations more effective; Table~\ref{table:avgcachedcovgchanges} shows the average changes in coverage by slicer mode in this setting.

Enabling caching brought about a reduction in solver queries and execution time, as seen in Table~\ref{table:avgcachedquerytimechanges}. The ratio of number of queries made while executing the sliced versions of the programs to the number made while executing the originals also saw a significant increase; Table~\ref{table:avgcachedquerycomparisons} shows these changes, it should be noted that the averages again do not include the top and bottom values to reduce the influence of outliers. 
%as repetitious or highly similar path conditions required fewer solver calls. Segue, execution time was also reduced, as solver queries are the most time-intensive part of symbolic execution.  Table~\ref{table:avgcachedquerychanges} shows these comparisons by slicer mode. 

\begin{table*}
\centering
\begin{tabular}{||c|c|c|c||}
    \hline
    Slicer Mode & Avg Coverage Change & Avg Coverage Gain Case & Avg Coverage Loss Case\\
    \hline\hline
     2 & 112.59 & 136.60 & 74.66 \\
     \hline
     3 & 98.87 & 117.48 & 72.82 \\
     \hline
     4 & 112.56 & 131.23 & 74.98 \\
     \hline
     5 & 122.55 & 134.85 & 67.11 \\
     \hline
\end{tabular}
\Caption{Average changes in coverage by slicer mode for Coreutils, cache enabled.}
\label{table:avgcachedcovgchanges}
\end{table*}

\begin{table*}
\centering
\begin{tabular}{||c|c|c||}
    \hline
    Slicer Mode & Average \% of Non-Cached Queries & Average \% of Non-Cached Execution Time \\
    \hline\hline
     Overall & 42.66 & 42.12 \\
     \hline
     0 & 46.86 & 32.05 \\
     \hline
     2 & 44.30 & 45.69 \\
     \hline
     3 & 43.20 & 53.44 \\
     \hline
     4 & 42.72 & 40.20 \\
     \hline
     5 & 35.37 & 38.66\\
     \hline
\end{tabular}
\Caption{Average changes in queries and execution time by slicer mode with caching for Coreutils.}
\label{table:avgcachedquerytimechanges}
\end{table*}

\begin{table*}
\centering
\begin{tabular}{||c|c|c|c||}
    \hline
    Slicer Mode & Avg \% of Unsliced Queries & Avg \% of Unsliced Queries (Coverage Gain) & Avg \% of Unsliced Queries (Coverage Loss)\\
    \hline\hline
     Overall & 349.53 & 400.14 & 248.43\\
     \hline
     2 & 430.89 & 557.02 & 236.75\\
     \hline
     3 & 358.35 & 446.45 & 239.83\\
     \hline
     4 & 335.64 & 326.71 & 372.22\\
     \hline
     5 & 270.35 & 314.36 & 33.78\\
     \hline
\end{tabular}
\Caption{Average changes in queries by slicer mode for Coreutils, cache enabled.}
\label{table:avgcachedquerycomparisons}
\end{table*}