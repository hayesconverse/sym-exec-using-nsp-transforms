\section{Motivating Examples}
\label{section:motiv_ex}

This section provides several examples using small programs to demonstrate how programs can be optimized for symbolic execution using non-semantics-preserving transformations.
The program in Figure~\ref{fig:e1a} has two consecutive loops with identical loop conditions. Symbolic execution of this program using KLEE (with default settings) produces a test suite that provides 100\% code coverage. Note that the control flow of the second loop is independent from the program inputs. Thus, removing the second loop, as in Figure~\ref{fig:e1b}, makes symbolic execution faster without decreasing the test suite's coverage. Control-flow graphs for both programs can be seen in Figures~\ref{fig:dlcfg1} and \ref{fig:dlcfg2}, respectively. Making this alteration by hand reduces KLEE's execution time from 10.9 hours to 23 minutes, providing a 28X speedup, and reduces the number of queries issued to the SMT solver from 15495 to 6823, i.e. a 2.3X reduction. This result is achievable using one of our transformations, described in Section~\ref{sec:the-slicer}. 

\begin{figure}[t]
\center
\begin{tabular}[c]{cc}
\begin{minipage}{0.46\textwidth}
\center
\lstinputlisting[language=c]{double_loop.c}
\Caption{\label{fig:e1a} Program $p$ with two loops.}
\end{minipage}
&\begin{minipage}{0.46\textwidth}
\center
\lstinputlisting[language=c]{loop_removed.c}
\Caption{\label{fig:e1b} Program $p_{transform}$, equivalent to $p$ with the second loop removed.}
\end{minipage}
\end{tabular}
\end{figure}

\begin{figure}[!ht]
\center
\includegraphics[width=0.6\textwidth]{double_loop_sliced0_simple}
\Caption{\label{fig:dlcfg1} CFG for program $p$. }
\end{figure}

\begin{figure}[t]
\center
\includegraphics[width=0.35\textwidth]{double_loop_sliced1_simple}
\Caption{\label{fig:dlcfg2} CFG for program $p_{transform}$. }
\end{figure}
%TOEDIT: Added speedup.
The program in Figure~\ref{fig:e2a} has a conditional structure inside of a loop. However, multiple symbolic executions of the loop are not necessary for complete coverage of the code within it. By adding an unconditional return statement at the end of the loop, as in Figure~\ref{fig:e2b}, we can significantly reduce the complexity of the structure while still providing 100\% line coverage. The control-flow graphs for these programs (in Figures~\ref{fig:lscfg1} and \ref{fig:lscfg2}, respectively) show the change visually: the altered program's graph has no back edges. In this case, KLEE's execution time is reduced from 19.63 to 3.29 seconds, i.e., about a 6X reduction, and makes only 201 solver queries for the transformed program as opposed to the original's 1619, i.e., an 8X reduction. This is similarly achievable using one of our transformations.

\begin{figure}[!ht]
\center
\begin{tabular}[c]{cc}
\begin{minipage}{0.46\textwidth}
\center
\lstinputlisting[language=c]{loop_switch.c}
\Caption{\label{fig:e2a} Program $q$ with a conditional structure contained in a loop.}
\end{minipage}
&\begin{minipage}{0.46\textwidth}
\center
\lstinputlisting[language=c]{loop_killed.c}
\Caption{\label{fig:e2b} Program $q_{transform}$, equivalent to $q$ with the loop terminated after a single iteration.}
\end{minipage}
\end{tabular}
\end{figure}

\begin{figure}[!ht]
\center
\includegraphics[width=0.55\textwidth]{loop_switch_sliced0_simple}
\Caption{\label{fig:lscfg1} CFG for program $q$.}
\end{figure}

\begin{figure}[!ht]
\center
\includegraphics[width=0.55\textwidth]{loop_switch_sliced5_simple}
\Caption{\label{fig:lscfg2} CFG for program $q_{transform}$}
\end{figure}

These examples demonstrate that non-semantics-preserving transformations can preserve the quality of the test suites generated through symbolic execution. In the course of our experiments, we discovered that the transformations we devised can actually improve the quality of these test suites. Our specific results are discussed in Section~\ref{section:results}.